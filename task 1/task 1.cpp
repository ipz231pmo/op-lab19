﻿#include <stdio.h>
int main(){
	int *p, x = 5, y = 10, m[10] = {1};
	p = &y;
	printf("Y value printing using pointer: %d\n", *p);
	x = *p;
	printf("x after operation x=*p : %d\n", x);
	y += 7;
	printf("Pointer after operation y+=7 : %p\n", p);
	*p += 5;
	printf("Y value after operation *p+=5 : %d", y);
	return 0;
}