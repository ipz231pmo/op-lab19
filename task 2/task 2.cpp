﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <algorithm>
#include <cstdlib>
void swap(int* a, int* b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}
int main(){
    srand(time(0));
    int arraySize;
    printf("Enter size of Array: ");
    scanf("%d", &arraySize);
    int* array = (int*)malloc(sizeof(int)*arraySize);
    printf("array = { ");
    for (int i = 0; i < arraySize; i++){
        array[i] = rand() % 201 - 100;
        printf("%4d ", array[i]);
    }
    printf("}\n");
    printf("Array takes %d bytes of ram\n", sizeof(int) * arraySize);
    printf("Array has %d elements\n", arraySize);
    printf("First element has adress %p\nLast element has adress %p\n", array, array + arraySize - 1);
    for (int i = 0; i < arraySize / 2; i++)
        swap(array + i, array + arraySize - 1 - i);
    printf("array after rewriting = { ");
    for (int i = 0; i < arraySize; i++)
        printf("%4d ", array[i]);
    printf("}\n");
    free(array);
    return 0;
}