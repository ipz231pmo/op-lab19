﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <time.h>

int main() {
    srand(time(0));
    int n;
    printf("Enter size of array: "); scanf("%d", &n);
    int* arr = (int*)malloc(n * sizeof(int));
    printf("arr = { ");
    for (int i = 0; i < n; i++) {
        arr[i] = rand() % 11 - 10;
        printf("%4d ", arr[i]);
    }
    printf("}\n");

    for (int i = 0; i < n; i++)
        if (arr[i] == 0) {
            for (int j = i+1; j < n; j++)
                arr[j-1] = arr[j];
            arr = (int*)realloc(arr, (--n)*sizeof(int));
            break;
        }

    printf("Changing array...\narr = { ");
    for (int i = 0; i < n; i++)
        printf("%4d ", arr[i]);
    printf("}\n");

    for (int i = 1; i < n; i++) 
        if(arr[i]%2 == 0){
            arr = (int*)realloc(arr, (++n) * sizeof(int));
            for (int j = n - 1; j >= i + 2; j--) 
                arr[j] = arr[j - 1];
            arr[i+1] = arr[i - 1]+2;
            i++;
        }
    printf("Changing array...\narr = { ");
    for (int i = 0; i < n; i++)
        printf("%4d ", arr[i]);
    printf("}\n");
    free(arr);
    return 0;
}