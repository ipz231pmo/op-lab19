﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <time.h>
void swap(float* a, float* b) {
    float temp = *a;
    *a = *b;
    *b = temp;
}
int main() {
    srand(time(0));
    int n;
    printf("Enter size of array: "); scanf("%d", &n);
    float* arr = (float*)malloc(n * sizeof(float));

    printf("arr = { ");
    for (int i = 0; i < n; i++) {
        arr[i] = rand() % 201 / 100. - 1;
        printf("%4.2f ", arr[i]);
    }
    printf("}\n");

    for (int i = 2; i < n; i += 2)
        swap(&arr[i], &arr[i - 1]);

    printf("Changing array...\narr = { ");
    for (int i = 0; i < n; i++)
        printf("%4.2f ", arr[i]);
    printf("}\n");

    free(arr);
    return 0;
}